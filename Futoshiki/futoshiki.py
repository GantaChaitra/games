
space = '_'
DIGITS = '123456789'
def setup(filename):
    lines = [line.strip() for line in open(filename)]
    size = 1 + len(lines[0]) // 2
    digits = DIGITS[:size]
    square = [[digits] * size for i in range(size)]
    constraints = []
    for line_num, line in enumerate(lines):
        constraints.append(line)
        if line_num % 2 == 0:
            filled = line[::2]
            for j, occupied in enumerate(filled):
                if occupied != space:
                    square[line_num // 2][j] = occupied
    return square, constraints


def transform_constraints(constraints):
    constraint_triples = []
    for i,constraint in enumerate(constraints):
        r = i // 2
        for j,ch in enumerate(constraint):
            c = j // 2
            if ch in "<>":
                constraint_triples.append(((r,c),ch,(r,c + 1)))
            elif ch in "v":
                ch = "<" if ch == "^" else ">"
                constraint_triples.append(((r,c),ch,(r + 1,c)))
    return constraint_triples


def update_grid(grid):
    size = len(grid[0])
    for i in range(size):
        for j in range(size):
            row,col = (i,j)
            if len(grid[row][col]) ==  1:
                size = len(grid[row])
                ch = grid[row][col]
                for r in range(size):
                    if r != row:
                        grid[r][col] = grid[r][col].replace(ch,"")
                for c in range(size):
                    if c != col:
                        grid[row][c] = grid[row][c].replace(ch,"")
    return grid
def update_cell_in_grid(grid):
    size = len(grid[0])
    for i in range(size):
        line = ''.join(grid[i])
        for num in set(line):
            if line.count(num) == 1:
                for j in range(size):
                    if num in grid[i][j]:
                        grid[i][j] = num
    return grid
def update_constraints(grid,constraints):
    size = len(grid[0])
    for constraint in constraints:
        lhs,rel,rhs = constraint
        if len(set(lhs) & set(rhs)) == 0:
            continue
        if rel == '>':
            rhs,lhs = lhs,rhs
        r1,c1 = lhs
        r2,c2 = rhs
        grid[r1][c1] = grid[r1][c1].replace(max(grid[r1][c1]),"")
        grid[r2][c2] = grid[r2][c2].replace(min(grid[r2][c2]),"")
    return grid

grid,constraints = setup("grid.txt")
print(grid)
print(constraints)
constraints = transform_constraints(constraints)
print(constraints)
grid = update_grid(grid)
print(grid)
grid = update_constraints(grid,constraints)
print(grid)
grid = update_constraints(grid,constraints)
