from array import *
def across(size,lis):
    across = []
    t_lis = []
    for i in range(size[0]):
        if(len(t_lis) > 0):
            across.append("".join(t_lis))
        t_lis.clear()
        for j in range(size[1]):
            if lis[i][j] != '*':
                t_lis.append(lis[i][j])
            else:
                if (len(t_lis) > 0):
                    across.append("".join(t_lis))
                t_lis.clear()
        if(i == size[0] - 1 and j == size[1] - 1 and len(t_lis) != 0):
            across.append("".join(t_lis))
            t_lis.clear()
    return across
def down(size,lis):
    down = []
    t_lis = []        
    for j in range(size[1]):
        if(len(t_lis) > 0):
            down.append("".join(t_lis))
        t_lis.clear()
        for i in range(size[0]):
            if lis[i][j] != '*':
                t_lis.append(lis[i][j])
            else:
                if (len(t_lis) > 0):
                    down.append("".join(t_lis))
                t_lis.clear()
                break
        if(i == size[0] - 1 and j == size[1] - 1 and len(t_lis) != 0):
            down.append("".join(t_lis))
            t_lis.clear()
    for i in range(size[0]):
        temp = i
        for j in range(size[1]):
            if (lis[temp][j] == '*'):
                for x in range(i+1,size[0]):
                    if (lis[x][j] != '*'):
                        t_lis.append(lis[x][j])
                    else:
                        if(len(t_lis) > 0):
                            down.append("".join(t_lis))
                        t_lis.clear()
                        break
                    if(x == (size[0] - 1) and len(t_lis) > 0):
                        down.append("".join(t_lis))
                        t_lis.clear()
    return down
def numbers(size,lis):
    across_num =[]
    down_num =[]
    num = 0
    for i in range(size[0]):
        for j in range(size[1]):
            if(lis[i][j] != '*'):
                if (i == 0):
                    if (j == 0):
                        num += 1
                        across_num.append(num)
                        down_num.append(num)
                    else:
                        if(lis[i+1][j] != '*'):
                            num += 1
                            down_num.append(num)
                            if(lis[i][j-1] == '*'):
                                across_num.append(num)
                elif(i>0 and 1<(size[0]-1)):
                    if(j ==0):
                        if(lis[i-1][j] == '*'):
                            num += 1
                            down_num.append(num)
                            if(lis[i][j+1] != '*'):
                                across_num.append(num)
                        elif(lis[i][j+1] != '*'):
                            num +=1
                            across_num.append(num)
                    else:
                        if(lis[i-1][j] =='*'):
                            num += 1
                            down_num.append(num)
                            if(lis[i][j-1] == '*'):
                               across_num.append(num)
                        elif(lis[i][j-1] == '*'):
                            num += 1
                            across_num.append(num)
    num_lis=[]
    num_lis.append(across_num)
    num_lis.append(down_num)
    return num_lis
size = [int(x) for x in input().split(" ")]
lis =[]
for i in range(size[0]):
    x = [j for j in input()]
    lis.append(x)
across = across(size,lis)
down = down(size,lis)
num_lis = numbers(size,lis)
across_num = num_lis[0]
down_num = num_lis[1]
print("puzzle #1")
print("Across")
for i in range(len(across)):
    print("  {across_num[i]}.{across[i]}")
print("Down")
for i in range(len(down)):
    print("  {down_num[i]}.{down[i]}")
